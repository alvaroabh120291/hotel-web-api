INSERT INTO categoria_cuarto (nombre,descripcion,precio)
    VALUES ('Individual', 'Ideal para quienes viajan solos.',50.0);

INSERT INTO cuarto (numero, descripcion,categoria)
    VALUES(1,'Vista a la piscina',1);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(2,'Remodelado recientemente',1);

INSERT INTO huesped (nombre,email,telefono) 
    values('Alvaro','alvaro@gmail.com','87654542');
INSERT INTO huesped (nombre,email,telefono) 
    values('Karla','karla@gmail.com','82720753');
INSERT INTO huesped (nombre,email,telefono) 
    values('Martha','martha@gmail.com','89280128');

Insert into reservacion (desde,hasta,cuarto,huesped) 
values (parsedatetime('2017-02-01','yyyy-MM-dd'),parsedatetime('2017-02-02','yyyy-MM-dd'),1,1);
Insert into reservacion (desde,hasta,cuarto,huesped) 
values (parsedatetime('2017-03-01','yyyy-MM-dd'),parsedatetime('2017-03-03','yyyy-MM-dd'),2,3);

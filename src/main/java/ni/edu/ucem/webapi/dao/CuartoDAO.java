package ni.edu.ucem.webapi.dao;

import java.util.List;

import ni.edu.ucem.webapi.modelo.Cuarto;

public interface CuartoDAO 
{
    public Cuarto obtenerPorId(final int pId);

    public int contar(final int offset, final int limit,final int numero,final String descripcion, final String search);
    
    public int contarPorCategoria(final int pCategoriaId,final int offset,final int limit,final int numero,final String descripcion,final String search);
    
    public List<Cuarto> obtenerTodos(final int offset, final int limit, final int numero, final String descripcion,final String search,final String sort,String sortOrder,String fields);

    public List<Cuarto> obtenerTodosPorCategoriaId(final int pCategoriaId, final int offset, final int limit, final int numero, final String descripcion,final String search,final String sort,String sortOrder,String fields);

    public void agregar(final Cuarto pCuarto);

    public void guardar(final Cuarto pCuarto);

    public void eliminar(final int pId);
}

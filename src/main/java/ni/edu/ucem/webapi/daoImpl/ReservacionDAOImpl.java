/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.daoImpl;

import java.util.List;
import ni.edu.ucem.webapi.dao.ReservacionDAO;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Reservacion;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author abarrera
 */
@Repository
public class ReservacionDAOImpl implements ReservacionDAO{
    
    private final JdbcTemplate jdbcTemplate;
    
    public ReservacionDAOImpl(final JdbcTemplate jdbcTemplate){
        this.jdbcTemplate=jdbcTemplate;
    }

    @Override
    public Reservacion obtenerPorId(int pId) {
        String sql = "select * from reservacion where id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{pId}, 
                new BeanPropertyRowMapper<Reservacion>(Reservacion.class));
    }
    
    @Override
    public Huesped obtenerHuesped(int pId){
        String sql = "select * from huesped where id= ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{pId},
                new BeanPropertyRowMapper<Huesped>(Huesped.class));
    }

    @Override
    public void guardar(Reservacion reservacion) {
        final String sql = new StringBuilder()
         .append("INSERT INTO reservacion")
         .append(" ")
         .append("(desde, hasta, cuarto, huesped)")
         .append(" ")
         .append("VALUES (?, ?, ?, ?)")
         .toString();
        
        final Object[] parametros = new Object[4];
        parametros[0] = reservacion.getDesde();
        parametros[1] = reservacion.getHasta();
        parametros[2] = reservacion.getCuarto();
        parametros[3] = reservacion.getHuesped();
        this.jdbcTemplate.update(sql,parametros);
    }
    
    @Override
    public Reservacion verificarReservacion(String desde, String hasta, int cuarto) {
        final String sql = "select * from reservacion where desde >= ? and hasta <= ? and cuarto = ?";
        Reservacion reservacion;
        try {
            reservacion = jdbcTemplate.queryForObject(sql, new Object[]{desde, hasta, cuarto},
                    new BeanPropertyRowMapper<Reservacion>(Reservacion.class));
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            reservacion = null;
        }
        return reservacion;
    }
    
    @Override
    public List<Cuarto> consultarCuartoReservado(final String desde, final String hasta, final int categoria,final int offset, final int limit) {

        String sql;

            if (categoria == 0) {
                sql = new StringBuilder()
                        .append("select * from cuarto where id not in (Select cuarto from reservacion")
                        .append(" where desde >= '").append(desde).append("' and hasta <= '").append(hasta).append("')")
                        .append("offset ").append(offset).append(" limit ").append(limit)
                        .toString();
            } else {
                sql = new StringBuilder()
                        .append("select * from cuarto where id not in (Select cuarto from reservacion")
                        .append(" where desde >= '").append(desde).append("' and hasta <= '").append(hasta).append("')")
                        .append(" AND categoria=").append(categoria)
                        .append("offset ").append(offset).append(" limit ").append(limit)
                        .toString();
            }
        
        System.out.println(sql);
        List<Cuarto> cuarto = this.jdbcTemplate.query(sql, new Object[]{},
                new BeanPropertyRowMapper<Cuarto>(Cuarto.class));
        //System.out.println(cuarto);
        return cuarto;
    }
    
}

package ni.edu.ucem.webapi.daoImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ni.edu.ucem.webapi.dao.CuartoDAO;
import ni.edu.ucem.webapi.modelo.Cuarto;

@Repository
public class CuartoDAOImpl implements CuartoDAO 
{
    private final JdbcTemplate jdbcTemplate;
   
    @Autowired
    public CuartoDAOImpl(final JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Cuarto obtenerPorId(final int pId) 
    {
        String sql = "select * from cuarto where id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{pId}, 
                new BeanPropertyRowMapper<Cuarto>(Cuarto.class));
    }
    
    @Override
    public int contar(final int pOffset, final int pLimit, final int numero, String descripcion, String search)
    {
        //final String sql = "select count(*) from cuarto";
         StringBuilder sql = new StringBuilder();
        sql.append("select count(*) ")
        .append("from cuarto ")
        .append("where (").append(numero).append("=0 or numero=").append(numero).append(") ")
        .append(" and ('").append(descripcion).append("'='' or descripcion like '%").append(descripcion).append("%' )")
        .append(" and ('").append(search).append("'='' or cast(numero as varchar) like '%").append(search).append("%' or cast(categoria as varchar) like '%").append(search).append("%' or descripcion like '%").append(search).append("%')")
        .append("offset ").append(pOffset)
        .append(" limit ").append(pLimit);
        return this.jdbcTemplate.queryForObject(sql.toString(), Integer.class);
    }
    
    @Override
    public int contarPorCategoria(final int categoriaId,final int pOffset, final int pLimit, final int numero, String descripcion, String search)
    {
        //final String sql = "select count(*) from cuarto where categoria= ?";
        StringBuilder sql = new StringBuilder();
        sql.append("select count(*) ")
        .append("from cuarto ")
        .append("where (").append(numero).append("=0 or numero=").append(numero).append(") ")
        .append(" and (").append(categoriaId).append("=0 or categoria=").append(categoriaId).append(") ")
        .append(" and ('").append(descripcion).append("'='' or descripcion like '%").append(descripcion).append("%' )")
        .append(" and ('").append(search).append("'='' or cast(numero as varchar) like '%").append(search).append("%' or cast(categoria as varchar) like '%").append(search).append("%' or descripcion like '%").append(search).append("%')")
        .append("offset ").append(pOffset)
        .append(" limit ").append(pLimit);
        
        //return this.jdbcTemplate.queryForObject(sql, new Object[]{categoriaId} ,Integer.class);
        return this.jdbcTemplate.queryForObject(sql.toString(), Integer.class);
    }

    @Override
    public List<Cuarto> obtenerTodos(final int pOffset, final int pLimit,final int numero, String descripcion, String search, String sort, String sortOrder,String fields) 
    {
        //String sql = "select * from cuarto where (?=0 or ) offset ? limit ?";
        StringBuilder sql = new StringBuilder();
        sql.append("select ").append(fields)
        .append(" from cuarto ")
        .append(" where (").append(numero).append("=0 or numero=").append(numero).append(") ")
        .append(" and ('").append(descripcion).append("'='' or descripcion like '%").append(descripcion).append("%' )")
        .append(" and ('").append(search).append("'='' or cast(numero as varchar) like '%").append(search).append("%' or cast(categoria as varchar) like '%").append(search).append("%' or descripcion like '%").append(search).append("%')")
        .append(" order by ").append(sort).append(" ").append(sortOrder)
        .append(" offset ").append(pOffset)
        .append(" limit ").append(pLimit);
        
        System.out.println("Query " + sql.toString());
        
        return this.jdbcTemplate.query(sql.toString(), 
                //new Object[]{pOffset, pLimit},
                new BeanPropertyRowMapper<Cuarto>(Cuarto.class));
    }

    @Override
    public List<Cuarto> obtenerTodosPorCategoriaId(int pCategoriaId, int pOffset, int pLimit, int numero, String descripcion, String search,String sort,String sortOrder,String fields) 
    {
        //final String sql = "select * from cuarto where categoria = ? offset ? limit ?";
        
        StringBuilder sql = new StringBuilder();
        sql.append("select ").append(fields)
        .append(" from cuarto ")
        .append(" where (").append(numero).append("=0 or numero=").append(numero).append(") ")
        .append(" and (").append(pCategoriaId).append("=0 or categoria=").append(pCategoriaId).append(") ")
        .append(" and ('").append(descripcion).append("'='' or descripcion like '%").append(descripcion).append("%' )")
        .append(" and ('").append(search).append("'='' or cast(numero as varchar) like '%").append(search).append("%' or cast(categoria as varchar) like '%").append(search).append("%' or descripcion like '%").append(search).append("%')")
        .append(" order by ").append(sort).append(" ").append(sortOrder)
        .append(" offset ").append(pOffset)
        .append(" limit ").append(pLimit);
        
        System.out.println("Query " + sql.toString());
        
        return this.jdbcTemplate.query(sql.toString(), 
                //new Object[]{pCategoriaId, pOffset, pLimit},
                new BeanPropertyRowMapper<Cuarto>(Cuarto.class));
    }

    @Override
    public void agregar(final Cuarto pCuarto) 
    {
        final String sql = new StringBuilder()
                .append("INSERT INTO cuarto")
                .append(" ")
                .append("(numero, descripcion, categoria)")
                .append(" ")
                .append("VALUES (?, ?, ?)")
                .toString();
        final Object[] parametros = new Object[3];
        parametros[0] = pCuarto.getNumero();
        parametros[1] = pCuarto.getDescripcion();
        parametros[2] = pCuarto.getCategoria();
        this.jdbcTemplate.update(sql,parametros);
        
    }

    @Override
    public void guardar(final Cuarto pCuarto) 
    {        
       final String sql = new StringBuilder()
                .append("UPDATE cuarto")
                .append(" ")
                .append("set numero = ?")
                .append(",descripcion = ?")
                .append(",categoria = ?")
                .append(" ")
                .append("where id = ?")
                .toString();
        final Object[] parametros = new Object[4];
        parametros[0] = pCuarto.getNumero();
        parametros[1] = pCuarto.getDescripcion();
        parametros[2] = pCuarto.getCategoria();
        parametros[3] = pCuarto.getId();
        this.jdbcTemplate.update(sql,parametros);
    }

    @Override
    public void eliminar(final int pId) 
    {
        final String sql = "delete from cuarto where id = ?";
        this.jdbcTemplate.update(sql, new Object[]{pId});
    }
}

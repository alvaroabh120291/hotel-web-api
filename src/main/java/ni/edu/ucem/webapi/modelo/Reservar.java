/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.modelo;

import java.sql.Date;

/**
 *
 * @author abarrera
 */
public class Reservar<T> {
    
    private int id;
    private String desde;
    private String hasta;
    private Cuarto cuarto;
    private Huesped huesped;
    
    public Reservar(){
        
    }
    
    public Reservar(int id, String desde, String hasta, Cuarto cuarto, Huesped huesped){
        
        this.id=id;
        this.desde= desde;
        this.hasta= hasta;
        this.cuarto=cuarto;
        this.huesped=huesped;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesde() {
        return desde;
    }

    public void setDesde(String desde) {
        this.desde = desde;
    }

    public String getHasta() {
        return hasta;
    }

    public void setHasta(String hasta) {
        this.hasta = hasta;
    }

    public Cuarto getCuarto() {
        return cuarto;
    }

    public void setCuarto(Cuarto cuarto) {
        this.cuarto = cuarto;
    }

    public Huesped getHuesped() {
        return huesped;
    }

    public void setHuesped(Huesped huesped) {
        this.huesped = huesped;
    }
    
}

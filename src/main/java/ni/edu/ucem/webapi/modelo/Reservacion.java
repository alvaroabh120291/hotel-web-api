/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.modelo;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Date;

/**
 *
 * @author abarrera
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Reservacion {
    
    private String  desde;
    private String hasta;
    private int cuarto;
    private int huesped;
    
    public Reservacion(){
        
    }
    
    public Reservacion(int id,String desde,String hasta,int cuarto,int huesped){
        this.id=id;
        this.desde=desde;
        this.hasta=hasta;
        this.cuarto=cuarto;
        this.huesped=huesped;
    }
    
    private int id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDesde() {
        return desde;
    }

    public void setDesde(String desde) {
        this.desde = desde;
    }

    public String getHasta() {
        return hasta;
    }

    public void setHasta(String hasta) {
        this.hasta = hasta;
    }

    public Integer getCuarto() {
        return cuarto;
    }

    public void setCuarto(Integer cuarto) {
        this.cuarto = cuarto;
    }

    public Integer getHuesped() {
        return huesped;
    }

    public void setHuesped(Integer huesped) {
        this.huesped = huesped;
    }

    
}

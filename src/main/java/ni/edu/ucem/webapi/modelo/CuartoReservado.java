/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.modelo;

import java.util.List;

/**
 *
 * @author abarrera
 */
public class CuartoReservado {
    
    private String desde;
    private String hasta;
    private List<Cuarto> data;
    
    public CuartoReservado(final String desde, final String hasta, final List<Cuarto> data){
        this.desde=desde;
        this.hasta=hasta;
        this.data=data;
    }

    public String getDesde() {
        return desde;
    }

    public void setDesde(String desde) {
        this.desde = desde;
    }

    public String getHasta() {
        return hasta;
    }

    public void setHasta(String hasta) {
        this.hasta = hasta;
    }

    public List<Cuarto> getData() {
        return data;
    }

    public void setData(List<Cuarto> data) {
        this.data = data;
    }
    
}

package ni.edu.ucem.webapi.serviceImpl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ni.edu.ucem.webapi.dao.CategoriaCuartoDAO;
import ni.edu.ucem.webapi.dao.CuartoDAO;
import ni.edu.ucem.webapi.modelo.CategoriaCuarto;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.service.InventarioService;

@Service
public class InventarioServiceImpl implements InventarioService 
{
    private final CategoriaCuartoDAO categoriaCuartoDAO;
    private final CuartoDAO cuartoDAO;
    
    public InventarioServiceImpl(final CategoriaCuartoDAO categoriaCuartoDAO,
            final CuartoDAO cuartoDAO)
    {
        this.categoriaCuartoDAO = categoriaCuartoDAO;
        this.cuartoDAO = cuartoDAO;
    }
    @Transactional
    @Override
    public void agregarCategoriaCuarto(final CategoriaCuarto pCategoriaCuarto) 
    {
        this.categoriaCuartoDAO.agregar(pCategoriaCuarto);
    }

    @Transactional
    @Override
    public void guardarCategoriaCuarto(final CategoriaCuarto pCategoriaCuarto) 
    {
        if(pCategoriaCuarto.getId() < 1)
        {
            throw new IllegalArgumentException("La categoría del cuarto no existe");
        }
        this.categoriaCuartoDAO.guardar(pCategoriaCuarto);
    }

    @Transactional
    @Override
    public void eliminarCategoriaCuarto(final int pId) 
    {
        if(pId < 1)
        {
            throw new IllegalArgumentException("ID invalido. Debe ser mayor a cero");
        }
        this.categoriaCuartoDAO.eliminar(pId);
    }

    @Override
    public CategoriaCuarto obtenerCategoriaCuarto(final int pId) 
    {
        return this.categoriaCuartoDAO.obtenerPorId(pId);
    }

    @Override
    public Pagina<CategoriaCuarto> obtenerTodosCategoriaCuartos(Filtro paginacion, String nombre, String descripcion,double precio, String search,String sort, String sortOrder,String fields) 
    {
        //return this.categoriaCuartoDAO.obtenerTodos(paginacion.getOffset(),paginacion.getLimit(),nombre,descripcion,precio,search,sort,sortOrder,fields);
        if(!(sortOrder.equals("asc") || sortOrder.equals("desc"))){
           sortOrder="asc";
        }
        
        if(!fields.equals("*")){
            String[] columnasFields= fields.split(",");
            ArrayList<String> columnasVerificadas = new ArrayList<String>();


            for(int i=0;i<columnasFields.length;i++){
                for(Field f : CategoriaCuarto.class.getDeclaredFields()){
                    if(columnasFields[i].equals(f.getName())){
                        columnasVerificadas.add(f.getName());
                    }
                }
            }

            fields= String.join(",", columnasVerificadas);
            System.out.println(fields);
        }
        
         if(!sort.equals("id")){
            String sortValido="";
            for(Field f : CategoriaCuarto.class.getDeclaredFields()){
                if(sort.equals(f.getName()))
                    sortValido=f.getName();
            }
            
            if(!sort.equals(sortValido))
                sort="id";
        }
        
        List<CategoriaCuarto> categorias;
        final int count = this.categoriaCuartoDAO.contar(paginacion.getOffset(),paginacion.getLimit(),nombre,descripcion,precio,search);
        if(count > 0)
        {
            categorias = this.categoriaCuartoDAO.obtenerTodos(paginacion.getOffset(),
                    paginacion.getLimit(), nombre, descripcion,precio,search,sort,sortOrder,fields);
        }
        else
        {
            categorias = new ArrayList<CategoriaCuarto>();
        }
        return new Pagina<CategoriaCuarto>(categorias, count,  paginacion.getOffset(), paginacion.getLimit());
    }
    
    @Transactional
    @Override
    public void agregarCuarto(final Cuarto pCuarto) 
    {
        this.cuartoDAO.agregar(pCuarto);

    }
    
    @Transactional
    @Override
    public void guardarCuarto(final Cuarto pCuarto) 
    {
        if(pCuarto.getId() < 1)
        {
            throw new IllegalArgumentException("El cuarto no existe");
        }
        this.cuartoDAO.guardar(pCuarto);
    }
    
    @Transactional
    @Override
    public void eliminarCuarto(final int pId) 
    {
        if(pId < 1)
        {
            throw new IllegalArgumentException("ID invalido. Debe ser mayor a cero");
        }
        this.cuartoDAO.eliminar(pId);
    }

    @Override
    public Cuarto obtenerCuarto(final int pId) 
    {
        if (pId < 0) 
        {
            throw new IllegalArgumentException("ID inválido. debe ser mayor a cero.");
        }
        return this.cuartoDAO.obtenerPorId(pId); 
    }

    @Override
    public Pagina<Cuarto> obtenerTodosCuarto(Filtro paginacion, Integer numero, String descripcion, String search,String sort, String sortOrder,String fields) 
    {
        if(!(sortOrder.equals("asc") || sortOrder.equals("desc"))){
           sortOrder="asc";
        }
        
        if(!fields.equals("*")){
            String[] columnasFields= fields.split(",");
            ArrayList<String> columnasVerificadas = new ArrayList<String>();


            for(int i=0;i<columnasFields.length;i++){
                for(Field f : Cuarto.class.getDeclaredFields()){
                    if(columnasFields[i].equals(f.getName())){
                        columnasVerificadas.add(f.getName());
                    }
                }
            }

            fields= String.join(",", columnasVerificadas);
            System.out.println(fields);
        }
        
        if(!sort.equals("id")){
            String sortValido="";
            for(Field f : Cuarto.class.getDeclaredFields()){
                if(sort.equals(f.getName()))
                    sortValido=f.getName();
            }
            
            if(!sort.equals(sortValido))
                sort="id";
        }
               
        List<Cuarto> cuartos;
        final int count = this.cuartoDAO.contar(paginacion.getOffset(),paginacion.getLimit(),numero,descripcion,search);
        if(count > 0)
        {
            cuartos = this.cuartoDAO.obtenerTodos(paginacion.getOffset(),
                    paginacion.getLimit(), numero, descripcion,search,sort,sortOrder,fields);
        }
        else
        {
            cuartos = new ArrayList<Cuarto>();
        }
        return new Pagina<Cuarto>(cuartos, count,  paginacion.getOffset(), paginacion.getLimit());
    }

    @Override
    public Pagina<Cuarto> obtenerTodosCuartoEnCategoria(final int pCategoriaCuartoId, final Filtro paginacion, Integer numero, String descripcion,String search,String sort,String sortOrder,String fields)
    {
        if(!(sortOrder.equals("asc") || sortOrder.equals("desc"))){
           sortOrder="asc";
        }
        
        if(!fields.equals("*")){
            String[] columnasFields= fields.split(",");
            ArrayList<String> columnasVerificadas = new ArrayList<String>();


            for(int i=0;i<columnasFields.length;i++){
                for(Field f : Cuarto.class.getDeclaredFields()){
                    if(columnasFields[i].equals(f.getName())){
                        columnasVerificadas.add(f.getName());
                    }
                }
            }

            fields= String.join(",", columnasVerificadas);
            System.out.println(fields);
        }
        
         if(!sort.equals("id")){
            String sortValido="";
            for(Field f : Cuarto.class.getDeclaredFields()){
                if(sort.equals(f.getName()))
                    sortValido=f.getName();
            }
            
            if(!sort.equals(sortValido))
                sort="id";
        }
        
        final int count = this.cuartoDAO.contarPorCategoria(pCategoriaCuartoId,paginacion.getOffset(),paginacion.getLimit(),numero,descripcion,search);
        List<Cuarto> cuartos = null;
        if(count > 0)
        {
            cuartos = this.cuartoDAO.obtenerTodosPorCategoriaId(pCategoriaCuartoId, paginacion.getOffset(),
                    paginacion.getLimit(),numero,descripcion,search,sort,sortOrder,fields);
        }
        else
        {
            cuartos = new ArrayList<Cuarto>();
        }
        return new Pagina<Cuarto>(cuartos, count,  paginacion.getOffset(), paginacion.getLimit());
    }
}

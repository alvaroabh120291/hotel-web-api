/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.serviceImpl;

import java.util.List;
import ni.edu.ucem.webapi.dao.ReservacionDAO;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.service.DisponibilidadService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abarrera
 */
@Service
public class DisponibilidadServiceImpl implements DisponibilidadService {
    
    private final ReservacionDAO reservacionDAO;
    
    public DisponibilidadServiceImpl(final ReservacionDAO reservacionDAO){
        this.reservacionDAO=reservacionDAO;
    }

    @Override
    public Reservacion obtenerReserva(int id) {
        return this.reservacionDAO.obtenerPorId(id);
    }

    @Transactional
    @Override
    public void guardarReservacion(Reservacion reservacion) {
        this.reservacionDAO.guardar(reservacion);
    }
    
    @Override
    public Huesped obtenerHuesped(int id){
        return this.reservacionDAO.obtenerHuesped(id);
    }
    
    @Override
    public Reservacion verificarReservacion(String desde, String hasta, int cuarto){
        return this.reservacionDAO.verificarReservacion(desde, hasta, cuarto);
    }
    
    @Override
    public List<Cuarto> consultarCuartoReservado(String desde, String hasta, int categoria,int offset, int limit){
        return this.reservacionDAO.consultarCuartoReservado(desde, hasta, categoria,offset,limit);
    }
    
}

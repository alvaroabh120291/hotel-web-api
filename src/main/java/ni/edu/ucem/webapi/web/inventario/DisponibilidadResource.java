/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.web.inventario;

import java.text.SimpleDateFormat;
import javax.validation.Valid;
import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.core.ApiResponse.Status;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.modelo.Reservar;
import ni.edu.ucem.webapi.service.DisponibilidadService;
import ni.edu.ucem.webapi.service.InventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author abarrera
 */
@RestController
@RequestMapping("/v1/disponibilidad/reservaciones")
public class DisponibilidadResource {
    private final DisponibilidadService disponibilidadService;
    private final InventarioService inventarioService;
    
    @Autowired
    public DisponibilidadResource(final DisponibilidadService disponibilidadService,final InventarioService inventarioService){
        this.disponibilidadService=disponibilidadService;
        this.inventarioService=inventarioService;
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces="application/json")
    public ApiResponse obtener(@PathVariable("id") final int id)
    {
        final Reservacion reservacion = this.disponibilidadService.obtenerReserva(id);
        
        Reservar reservar= new Reservar(reservacion.getId(),reservacion.getDesde(),reservacion.getHasta(),inventarioService.obtenerCuarto(reservacion.getCuarto()),disponibilidadService.obtenerHuesped(reservacion.getHuesped()));
        
        return new ApiResponse(Status.OK, reservar);
    }
    
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponse guardarReservacion(@Valid @RequestBody final Reservacion reservacion, BindingResult result){
        Status estado=Status.OK;
        
        if(result.hasErrors())
        {
            throw new IllegalArgumentException(result.getFieldError().getDefaultMessage());
        }
        
        if(disponibilidadService.verificarReservacion(reservacion.getDesde(), reservacion.getHasta(), reservacion.getCuarto())!= null){
            throw new IllegalArgumentException("Cuarto ya Reservado");
        }
        
        this.disponibilidadService.guardarReservacion(reservacion);
        
        return new ApiResponse(estado,reservacion);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.web.inventario;

import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.core.ApiResponse.Status;
import ni.edu.ucem.webapi.modelo.CuartoReservado;
import ni.edu.ucem.webapi.service.DisponibilidadService;
import ni.edu.ucem.webapi.service.InventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author abarrera
 */
@RestController
@RequestMapping("/v1/disponibilidad/cupos")
public class CuartoDisponibilidadResource {
    
    private final DisponibilidadService disponibilidadService;
    private final InventarioService inventarioService;
    
    @Autowired
    public CuartoDisponibilidadResource(final DisponibilidadService disponibilidadService,final InventarioService inventarioService){
        this.disponibilidadService=disponibilidadService;
        this.inventarioService=inventarioService;
    }
    
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ApiResponse consultaCuartos(
        @RequestParam(value = "desde", required = true) final String desde,
        @RequestParam(value = "hasta", required = true) final String hasta,
        @RequestParam(value = "categoria", required = false, defaultValue="0") final int categoria,
        @RequestParam(value = "offset", required = false, defaultValue="0")final int offset,
        @RequestParam(value = "limit", required = false, defaultValue="10")final int limit){
        
        
        CuartoReservado cuartoReservado=new CuartoReservado(desde,hasta,disponibilidadService.consultarCuartoReservado(desde, hasta, categoria,offset,limit));
        
        return new ApiResponse(Status.OK,cuartoReservado);
    }
}

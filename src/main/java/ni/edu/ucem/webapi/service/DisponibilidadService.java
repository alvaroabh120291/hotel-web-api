/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.service;

import java.util.List;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Reservacion;

/**
 *
 * @author abarrera
 */
public interface DisponibilidadService {
    
    public Reservacion obtenerReserva(final int id);
    
    public void guardarReservacion(final Reservacion reservacion);
    
    public Huesped obtenerHuesped(final int id);
    
    public Reservacion verificarReservacion(String desde, String hasta, int cuarto);
    
    public List<Cuarto> consultarCuartoReservado(String desde, String hasta, int categoria,int offset, int limit);
    
}
